import ChromeApi from "./chromeApi";

/**
 * Creates a Setting class.
 * @class
 */
class Setting {
  private domId: string = "";
  private url: string = "";
  private tags: string[] = [];
  private static instance: Setting;
  private chromeApi: ChromeApi = ChromeApi.getInstance();

  /** ------------------------------------------------------------
   * @constructor
   * @param  {string} domId [id for append]
  */
  constructor(_domId: string) {
    this.domId = _domId;
  }
  /** ------------------------------------------------------------
   * create instance of class
   * @static
   * @param  {String} domId [id of element we want to append]
   * @return {Setting}     [create class]
  */
  public static getInstance(domId: string): Setting {
    if (!this.instance) {
      this.instance = new Setting(domId);
    }
    return this.instance;
  }
  /** ------------------------------------------------------------
   * set new url
   * @private
   * @param  {HTMLElement} "P" [p element we set url as its text]
  */
  private handleUrlInput(urlElement: HTMLElement): void {
    const inputUrl = document.getElementById("url-input") as HTMLInputElement;
    if (inputUrl.value.length > 0) {
      // send url to background
      this.chromeApi.sendMessage({ url: inputUrl.value });
      // set url to storage
      this.chromeApi.setStorage({ url: inputUrl.value });
      // set url text to its element
      this.url = inputUrl.value;
      urlElement.textContent = inputUrl.value;
      // reset input value after submit
      inputUrl.value = "";
    }
  }
  /** ------------------------------------------------------------
   * Delete tag of animes tags
   * @private
   * @param  {Event} mouse event [event for element clicked]
  */
  private handleDeleteTag(e: Event): void {
    const clickedElement = e.target as HTMLElement;
    // validate click on the close span only
    if (clickedElement.className === "close") {
      const pickedElementId = clickedElement.closest("div").id;
      const pickedElement = document.getElementById(pickedElementId);
      // remove tag from tags and it element
      this.tags = this.tags.filter((t: string) => t != pickedElementId);
      pickedElement!.remove();
      // set storage with new tags
      this.chromeApi.setStorage({ filterTags: this.tags });
      // sync with background
      this.chromeApi.sendMessage({ data: "Handshake" });
    }
  }
  /** ------------------------------------------------------------
   * submit form for add tags
   * @private
   * @param  {Event} formEvent [event for when form submited]
  */
  private handleFormSubmit(inputTag: HTMLInputElement, e: Event): void {
    e.preventDefault();
    // not submit without value
    if (inputTag.value.length > 0) {
      // avoid duplicate tags
      if(!this.tags.includes(inputTag.value.toLowerCase())){
        const tagWrapper = document.querySelector(".tags");
        // set value and add it to wrapper
        const inputValue = inputTag.value.toLowerCase();
        tagWrapper?.insertAdjacentHTML("beforeend", this.renderTag(inputValue));
        this.tags.push(inputValue);
        // set storage with new tags
        this.chromeApi.setStorage({ filterTags: this.tags });
        // clear value for input tag
        inputTag.value = "";
        // sync with background
        this.chromeApi.sendMessage({ data: "Handshake" });
      }else {
        inputTag.value = "";
      }
    }
  }
  /** ------------------------------------------------------------
   * handle alert for switch button
   * @private
   * @param  {Event} e [event for checkbox input]
  */
  private handleAlert(e: Event) {
    const inputCheckbox = e.currentTarget as HTMLInputElement;
    if (inputCheckbox.checked) {
      this.chromeApi.setStorage({ notif: "on" });
      this.chromeApi.sendMessage({ data: "Handshake" });
    } else {
      this.chromeApi.setStorage({ notif: "off" });
      this.chromeApi.sendMessage({ data: "Handshake" });
    }
  }
  /** ------------------------------------------------------------
   * method to create innerhtml for tag.
   * @function renderTag
   * @private
   * @param {string} tag - [tag for filtering anime].
   * @returns {string} [innerHTML of li element]
   *
  */
  private renderTag(tag: string) {
    return `
    <div id="${tag}" class="tag">
      <span>${tag}</span>
      <span class="close">
        X
      </span>
    </div>
    `;
  }
  /** ------------------------------------------------------------
   * method to create innerhtml for setting.
   * @function renderHtml
   * @private
   * @param {string[]} tags - [list of tags].
   * @returns {string} [innerHTML of li element]
   *
  */
  private renderHtml(tags: string[]) {
    return `
    <div class="alert-wrapper" >
      <label class="toggle-switch">
        <input id="checkbox" type="checkbox"></input>
        <span>
          <i></i>
        </span>
      </label>
      <span class="text-alert" >Alert</span>
    </div>
      <div>
        <input id="url-input" type="text" placeholder="Enter Url" />
        <button id="add-url" >add</button>
        <p class="url" >No Url Added</p>
      </div>
      <form autocomplete="off" id="form-add-tag" >
        <input id="tag-input" type="text" placeholder="Enter Anime"/>
        <button type="submit">add</button>
      </form>
      <div class="tags">
        ${tags?.map((tag: string) => this.renderTag(tag)).join(" ")} 
      </div>
    `;
  }
  /** ------------------------------------------------------------
   * method to create innerhtml.
   * @function render
   * @param {string[]} tags - [list of tags].
   *
  */
  render(_tags: string[]=[],_alertMode) {
    this.tags = _tags;
    const appWrapper = document.querySelector(`.${this.domId}`);
    appWrapper!.innerHTML = this.renderHtml(this.tags);
    /*------------------- Add tags ----------------- */
    const form = document.getElementById("form-add-tag");
    const inputTag = form.querySelector("input");
    form?.addEventListener(
      "submit",
      this.handleFormSubmit.bind(this, inputTag)
    );
    /*------------------ delete Tags --------------- */
    const tagWrapper = document.querySelector(".tags");
    tagWrapper?.addEventListener("click", this.handleDeleteTag.bind(this));
    /*--------------------- Url -------------------- */
    const addUrlBtn = document.getElementById("add-url");
    const urlElement = document.querySelector(".url");
    this.chromeApi.getStorage(["url"]).then(({ url }) => {
      if (url) {
        this.url = url;
        urlElement.textContent = url;
      }
    });
    addUrlBtn?.addEventListener(
      "click",
      this.handleUrlInput.bind(this, urlElement)
    );
    /*--------------------- alert ------------------- */
    const switchInput = document.getElementById("checkbox") as HTMLInputElement;
    if(_alertMode === 'on'){
      switchInput.checked = true
    }
    switchInput.addEventListener("change", this.handleAlert.bind(this));
  }
}

export default Setting;

import ChromeApi from './chromeApi';
import AnimeList from './AnimeList';
import Setting from "./Setting";

/**
 * Creates View class.
 * @class 
 */
class View {
  setting: boolean = false;
  chromeApi: ChromeApi = ChromeApi.getInstance();
  private static instance: View;

  /** ------------------------------------------------------------  
   * create instance of class
   * @static
   * @return {View}     [create class]
  */
  public static getInstance(): View {
    if (!this.instance) {
      this.instance = new View();
    }
    return this.instance;
  }
  /** ------------------------------------------------------------   
   * swicth mode between setting and animelist
   * @private
   * @param  {AnimeList} AnimeList 
   * @param  {Setting} Setting
  */
  private handleSwitchMode(animeList: AnimeList, setting: Setting) {
    const swicthViewBtn = document.querySelector("i");
    if (this.setting) {
      // get filter tags from storage
      this.chromeApi.getStorage(["filterTags",'notif']).then((items: any) => {        
        setting.render(items.filterTags,items.notif);
        swicthViewBtn!.className = "fa fa-arrow-circle-left";
        this.setting = false;
      });
    } else {
      // get data list from storage
      this.chromeApi.getStorage(["dataList"]).then((items: any) => {
        animeList.render(items.dataList);
        swicthViewBtn!.className = "fa fa-cog";
        this.setting = true;
      });
    }
  }  
  /** ------------------------------------------------------------   
   * render both setting and list of anime
   * @param  {AnimeList} AnimeList 
   * @param  {Setting} Setting
  */
  render(animeList: AnimeList, setting: Setting): void {
    const swicthViewBtn = document.querySelector("i");
    this.handleSwitchMode(animeList, setting);
    swicthViewBtn?.addEventListener("click", () =>
      this.handleSwitchMode(animeList, setting)
    );
  }
}

export default View;
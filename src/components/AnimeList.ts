import ChromeApi from "./chromeApi";

interface IList {
  url: string;
  image: string;
  episode: string;
  title: string;
}

/**
 * Creates a new AnimeList.
 * @class 
 */
class AnimeList { 
  private domId: string = "";
  private url: string = "";
  private static instance: AnimeList;
  
  /** ------------------------------------------------------------
   * for initalize the class we need chrome api 
   * to find url that we use to redirect to anime page
   * and set domId as well
   * @constructor
   * @param  {string} domId [id for append]
  */
  constructor(_domId: string) {
    this.domId = _domId;
    const chromeApi=ChromeApi.getInstance();
    chromeApi.getStorage(["url"]).then(({ url }: any) => {
      if (url) {
        this.url = url;
      }
    });
  }
  /** ------------------------------------------------------------  
   * create instance of class
   * @static
   * @param  {String} domId [id of element we want to append]
   * @return {AnimeList}     [create class]
  */
  public static getInstance(domId: string): AnimeList {
    if (!this.instance) {
      this.instance = new AnimeList(domId);
    }
    return this.instance;
  }
  /** ------------------------------------------------------------
   * method to create innerhtml for anime list.
   * @function renderHtml
   * @private
   * @param {IList} list - [each item of list].
   * @returns {string} [innerHTML of li element]
   * 
  */
  private renderHtml(list: IList): string {
    return `
      <li class="list-item">
      <a target="_blank" href=${this.url}${list.url} >
        <div class="img-wrapper">
          <img
            class="img-list"
            src=${list.image}
          />
        </div>
        <div class="text-wrapper">
          <p class="text-wrapper-title">${list.title}</p>
          <p>${list.episode}</p>
        </div>
        </a>
      </li>
    `;
  }
  /** ------------------------------------------------------------   
   * render List of anime
   * @param  {IList[]} list [description]
  */
  render(list: IList[]): void {
    const animeListWrapper: HTMLElement | null = document.querySelector(
      `.${this.domId}`
    );
    // clear elements before each new render
    animeListWrapper!.innerHTML = "";
    // Create an ul element 
    const ul = document.createElement("ul");
    ul.setAttribute("class", "list-wrapper");
    ul.setAttribute("id", "list-wrapper");
    animeListWrapper?.appendChild(ul);
    // create lists
    if (list.length === 0) {
      const p = document.createElement("p");
      p.textContent = "There is no anime for now";
      p.className = "no-anime-text";
      ul.appendChild(p);
    } else {
      for (let i = 0; i < list.length; i++) {
        ul?.insertAdjacentHTML("beforeend", this.renderHtml(list[i]));
      }
    }
  }
}

export default AnimeList;

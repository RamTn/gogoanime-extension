import "./content.css";

/**
 * Creates a new Notif.
 * @class
 */
class Notif {
  /** ------------------------------------------------------------
   * Delete notif toastify
   * @private
   */
  private handleClose() {
    const notifWrapper = document.querySelector(".notif-wrapper");
    if (notifWrapper) {
      notifWrapper.remove();
    }
  }
  /** ------------------------------------------------------------
   * method to create innerhtml for notification toastify.
   * @function renderHtml
   * @private
   * @param {number} leng - [number of anime has realeased].
   * @returns {string} [innerHTML of notif element]
   *
   */
  private renderHtml(leng: number) {
    return `
      <div class="notif-wrapper" >
        <span>${leng} anime has released</span>
        <span class="close-notif" >X</span>
      <div>
    `;
  }
  /** ------------------------------------------------------------
   * method to create innerhtml.
   * @function render
   * @param {array} items - [list of new anime].
   *
   */
  render(items) {
    const docBody = document.body;
    const notifWrapper = document.querySelector(".notif-wrapper");
    if (notifWrapper) {
      notifWrapper.remove();
    }
    docBody.insertAdjacentHTML("beforebegin", this.renderHtml(items.length));
    const closeBtn = document.querySelector(".close-notif");
    closeBtn?.addEventListener("click", this.handleClose.bind(this));
  }
}

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  new Notif().render(message.findItems);
  return true;
});

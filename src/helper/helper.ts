import cheerio from 'cheerio';

export interface IList {
  url: string;
  image: string;
  episode: string;
  title: string;
}

export const filtersList = (list: IList[]=[], filterBy: string[]=[]) => {
  let fList: IList[] = [];
  list?.filter((item) =>
    filterBy?.map((i) => {
      if (item.title.toLowerCase().trim().indexOf(i.toLowerCase().trim()) !== -1) {
        fList.push(item);
      }
    })
  );
  return [...new Set(fList)];
};

export const getDataWithCheerio = (html: any) => {
  const articles: any = [];
  const $ = cheerio.load(html);
  $(".last_episodes", html)
  .find("li")
  .each(function (index, el) {
    const title = $(el).find("a").attr("title");
    const url = $(el).find("a").attr("href");
      const episode = $(el).find(".episode").text();
      const image = $(el).find("img").attr("src");
      articles.push({ image, title, url, episode });
    });
    
    return articles;
  };


export const timeInteval = (duration:number,callback:any)=>{
  let timer;
  timer=setInterval(()=>{
    callback()
  },duration)
  return timer
}

export const findChangesArrays = (formerlist=[], nextList=[]) => {
  let list = [];
  nextList.forEach((i, index) => {
    if (!formerlist.map((item) => item.title).includes(i.title)) {
      list.push(nextList[index]);
    }
  });
  return list;
};
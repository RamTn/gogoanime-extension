import {
  filtersList,
  timeInteval,
  getDataWithCheerio,
  IList,
  findChangesArrays,
} from "./helper/helper";
import ChromeApi from "./components/chromeApi";
import fetchAdapter from "@vespaiach/axios-fetch-adapter";
import axios from "axios";

const instance = axios.create({
  adapter: fetchAdapter,
});

let url = "";
let dataList: IList[] = [];
let timerId = null;
const chromeApi = ChromeApi.getInstance();

function fetchAnimeList() {
  return instance
    .request({
      method: "get",
      url,
      adapter: fetchAdapter,
    })
    .then((response) => {
      const html = response.data;
      const data = getDataWithCheerio(html);
      return data;
    })
    .catch((err) => console.log(err));
}

function handleNotification() {
  chrome.storage.sync.get(["notif", "cache", "filterTags"], function (items) {
    if (items.notif === "on") {
      fetchAnimeList().then((res) => {
        const findItems = findChangesArrays(
          items.cache,
          filtersList(res, items.filterTags)
        );
        chrome.tabs.query(
          { active: true, currentWindow: true },
          function (tabs) {
            if (tabs.length > 0) {
              if (findItems.length > 0) {
                chrome.tabs.sendMessage(
                  tabs[0]?.id,
                  { findItems },
                  function (response) {
                    if (chrome.runtime.lastError) {
                      return;
                    }
                  }
                );
              }
            }
          }
        );
      });
    }
  });
}

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
  if (message.url) {
    url = message.url;
  } else {
    chromeApi.getStorage(["url"]).then((items) => {
      url = items.url;
    });
  }

  chrome.storage.sync.get(["notif"], function (items) {
    if (items.notif === "on") {
      clearInterval(timerId);
      fetchAnimeList().then((res) => {
        chrome.storage.sync.set({
          cache: res,
        });
        timerId = timeInteval(300000, handleNotification);
      });
    } else {
      clearInterval(timerId);
    }
  });

  chrome.storage.sync.get(
    /* String or Array */ ["filterTags"],
    function (items) {
      fetchAnimeList().then((res) => {
        dataList = res;
        chrome.storage.sync.set({
          dataList: filtersList(dataList, items.filterTags),
        });
        chrome.runtime.sendMessage({
          data: filtersList(dataList, items.filterTags),
          filtersTag: items.filterTags,
        });
      });
    }
  );
});

class Implementation {
  static init() {
    fetchAnimeList().then((res) => {
      dataList = res;
    });
    chrome.storage.sync.get(["notif"], function (items) {
      if (items.notif === "on") {
        clearInterval(timerId);
        timerId = timeInteval(300000, handleNotification);
      }
    });
  }
}

Implementation.init();

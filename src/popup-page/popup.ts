import ChromeApi from '../components/chromeApi';
import AnimeList from '../components/AnimeList';
import Setting from '../components/Setting';
import View from '../components/View';
import "./popup.css";
/**
 * Creates Implementation.
 * @class 
 */
class Implementation {
  /** ------------------------------------------------------------  
   * Initial app
   * @static
  */
  static init() {
    // initial chrome api
    const chromeApi = ChromeApi.getInstance();
    // sync to background
    chromeApi.sendMessage({ data: "Handshake" });
    // listen to response
    chromeApi.onMessage().then((res: any) => {
      View.getInstance().render(
        AnimeList.getInstance("app-wrapper"),
        Setting.getInstance("app-wrapper")
      );
    });
  }
}

addEventListener("DOMContentLoaded", () => {
  Implementation.init();
});

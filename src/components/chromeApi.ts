/**
 * Creates a ChromeApi class.
 * @class 
 */
class ChromeApi {
  private static instance: ChromeApi;
  
  /** ------------------------------------------------------------  
   * create instance of class
   * @static
   * @return {ChromeApi}     [create class]
  */
  public static getInstance(): ChromeApi {
    if (!this.instance) {
      this.instance = new ChromeApi();
    }
    return this.instance;
  }
  /** ------------------------------------------------------------   
   * set storage
   * @param  {any} _items [the items we want to set it in storage]
  */
  setStorage(_items: any): void {
    chrome.storage.sync.set({ ..._items });
  }
  /** ------------------------------------------------------------   
   * get storage data
   * @param  {string[]} key [get data from storage base on array of keys]
   * @returns {promise}
  */
  getStorage(key: string[]):Promise<any> {
    return new Promise((resolve, reject) => {
      chrome.storage.sync.get(key, function (items) {
        resolve(items);
      });
    });
  }
  /** ------------------------------------------------------------   
   * send message 
   * @param  {any} payload [payload we send it]
  */
  sendMessage(payload: any) {
    chrome.runtime.sendMessage(payload);
  }
  /** ------------------------------------------------------------   
   * listener for listen to message on runtime
   * @returns {promise}
  */
  onMessage():Promise<any> {
    return new Promise((resolve, reject) => {
      chrome.runtime.onMessage.addListener(async function (message) {
        resolve(message);
      });
    });
  }
}

export default ChromeApi;